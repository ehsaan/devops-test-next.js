# Base image.
FROM nginx:stable-alpine

# Copy Nginx configs.

RUN ls -alh

# Copy app bundle.
COPY dist /usr/share/nginx/html

# Public ports.
EXPOSE 80
